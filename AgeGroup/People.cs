﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AgeGroup
{
    class People : IEnumerable, IAlgorithm
    {
        readonly Person[] persons;

        public People(Person[] _persons)
        {
            persons = _persons;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new PeopleEnumerator(persons);
        }

        public void Sort()
        {
            Array.Sort(persons);
        }

        public string SearchPeople(int Age)
        {
            int leftPosition = 0;
            int rightPosition = persons.Length - 1;
            int searchPosition = -1;
            while (leftPosition <= rightPosition)
            {
                int middlePosition = (rightPosition + leftPosition) / 2;
                if (persons[middlePosition].Age == Age)
                {
                    searchPosition = middlePosition;
                    break;
                }
                if (persons[middlePosition].Age < Age)
                    leftPosition = middlePosition + 1;
                else
                    rightPosition = middlePosition - 1;

            }

            if (searchPosition != -1)
                return persons[searchPosition].Name;
            else
                return "Человек не найден с данным возрастом";
        }
    }
}
