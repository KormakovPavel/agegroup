﻿using System;

namespace AgeGroup
{
    class Program
    {
        static void Main()
        {
            Person[] person = new Person[]
            {
                new Person { Name = "Максим", Age = 34 },
                new Person { Name = "Иван", Age = 23 },
                new Person { Name = "Ирина", Age = 21 },
                new Person { Name = "Кирилл", Age = 18 },
                new Person { Name = "Дима", Age = 20 },
                new Person { Name = "Алина", Age = 45 },
                new Person { Name = "Сергей", Age = 54 },
                new Person { Name = "Марина", Age = 65 },
                new Person { Name = "Александр", Age = 13 },
                new Person { Name = "Надежда", Age = 70 },
            };
            People people = new People(person);

            Console.WriteLine("Список людей:");
            foreach (Person p in people)
                Console.WriteLine($"{p.Name} - {p.Age}");
            Console.WriteLine();

            people.Sort();
            Console.WriteLine("Сортированный список людей:");
            foreach (Person p in people)
                Console.WriteLine($"{p.Name} - {p.Age}");
            Console.WriteLine();

            Console.Write("Введите возраст человека: ");
            Int32.TryParse(Console.ReadLine(), out int Age);
            Console.Write(people.SearchPeople(Age));
            Console.ReadLine();
        }

    }
}
