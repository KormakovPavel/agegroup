﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AgeGroup
{
    class PeopleEnumerator : IEnumerator
    {
        readonly Person[] people;
        int position = -1;
        public PeopleEnumerator(Person[] _people)
        {
            people = _people;
        }

        public object Current
        {
            get
            {
                if (position == -1 || position >= people.Length)
                    throw new InvalidOperationException();
                return people[position];
            }
        }

        public bool MoveNext()
        {
            if (position < people.Length - 1)
            {
                position++;
                return true;
            }
            else
                return false;
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
