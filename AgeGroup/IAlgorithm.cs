﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AgeGroup
{
    interface IAlgorithm
    {
        void Sort();
        string SearchPeople(int Age);
    }
}
