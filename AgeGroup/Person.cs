﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgeGroup
{
    class Person :  IComparable
    {
        public string Name { get; set; }
        public int Age { get; set; }
       
        public int CompareTo(object ObjectPerson)
        {
            if (ObjectPerson is Person person)
                return this.Age.CompareTo(person.Age);
            else
                throw new Exception("Невозможно сравнить два объекта");
        }
    }
}
